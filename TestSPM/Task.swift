//
//  Task.swift
//  TestSPM
//
//  Created by Salvador Lopez on 28/06/23.
//

import Foundation

struct Task{
    let id: Int64
    var name: String
    var date: Date
    var status: Bool
}
