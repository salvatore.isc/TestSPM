//
//  ViewController.swift
//  TestSPM
//
//  Created by Salvador Lopez on 28/06/23.
//

import UIKit
import MyStaticLibrary
import MyDynamicFramework
import MySecondDynamicLib


class ViewController: UIViewController {
    
    var taskDataStore: TaskDataStore!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //MyStaticLibrary.sayHello()
        //MyDynamicLibrary.sayHello()
        
        MySecondClass.sayBye()
        
        taskDataStore = TaskDataStore.shared
        
        //let id = taskDataStore.insertTask(name: "Estudiar Ingles", date: Date()+10000)
        //print("Se creo la tarea con el id: \(id!)")
        
        let tasks = taskDataStore.getAllTasks()
        tasks.forEach { task in
            print("ID: \(task.id)")
            print("Nombre: \(task.name)")
            print("Fecha: \(task.date)")
            print("Status: \(task.status)")
        }
    }


}

