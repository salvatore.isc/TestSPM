//
//  Practica.swift
//  TestSPM
//
//  Created by Salvador Lopez on 28/06/23.
//

import Foundation

//MARK: PRACTICA 4

/**
 Practica: 4
 Objetivo: Reforzar, el uso de Swift Package Manager con SQLite y el patron de diseño Facade.
 Requerimientos:
 
 Partiendo de la practica numero 2, en donde se listan medicamentos.
 Guardar una coleccion de medicamentos haciendo uso de SQLite y una clase Facade como intermediaria de la clase TaskDataStore.
 Puden nombrar la clase Facade como TaskDataStoreFacade.
 Adicional, validar que no se pierda el funcionamiento relacionado a la practica numero 2.
 */
