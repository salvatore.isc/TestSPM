//
//  TaskDataStore.swift
//  TestSPM
//
//  Created by Salvador Lopez on 28/06/23.
//

import Foundation
import SQLite

class TaskDataStore{
    
    //Nombre del directorio
    static let DIR_TASK_DB = "taskdb"
    //Nombre para el archivo de BD
    static let STORE_DB_NAME = "task.sqlite3"
    
    //Scheme Type
    private let tasks = Table("Task")
    
    //Columnas de la tabla
    private let id = Expression<Int64>("id")
    private let taskname = Expression<String>("name")
    private let date = Expression<Date>("date")
    private let status = Expression<Bool>("status")
    
    //Singleton
    static let shared = TaskDataStore()
    
    //Conexion a la BD SQLITE
    private var db: Connection? = nil
    
    //Crear ruta, Crear BD, realizar CNX
    private init(){
        // Directorio
        if let docDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let dirPath = docDir.appendingPathExtension(Self.DIR_TASK_DB)
            do{
                try FileManager.default.createDirectory(atPath: dirPath.path, withIntermediateDirectories: true)
                let dbPath = docDir.appendingPathComponent(Self.STORE_DB_NAME).path
                db = try Connection(dbPath)
                print("SQLite was created successfully...")
                createTable()
                dump(dbPath)
            }catch{
                print("Error: \(error)")
            }
        }
    }
    
    private func createTable(){
        guard let database = db else {
            return
        }
        do{
            try database.run(tasks.create{
                table in
                table.column(id, primaryKey: .autoincrement)
                table.column(taskname)
                table.column(date)
                table.column(status)
            })
            print("Table was created successfully...")
        }catch{
            print("Error: \(error)")
        }
    }
    
    func getAllTasks() -> [Task] {
        var tasks: [Task] = []
        
        guard let database = db else{
            return []
        }
        
        // Consulta de registros
        do {
            for task in try database.prepare(self.tasks){
                tasks.append(Task(id: task[id], name: task[taskname], date: task[date], status: task[status]))
            }
        }catch{
            print("Error: \(error)")
        }
        return tasks
    }
    
    func insertTask(name:String, date: Date) -> Int64? {
        guard let database = db else {
            return nil
        }
        
        let insert = tasks.insert(
            self.taskname <- name,
            self.date <- date,
            self.status <- false
        )
        
        dump(insert)
        
        do{
            let rowId = try database.run(insert) // "INSERT INTO Task (name, date, status) VALUES (?,?,?)",varName,"formatofecha",0
            return rowId
        }catch{
            print("Error: \(error)")
            return nil
        }
    }
    
}
